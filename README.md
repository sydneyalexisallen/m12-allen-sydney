Sources used to complete this assignment:

I used this week's lecture to figure out how to print only the first name in the array by using 'characters [0]'


https://www.w3schools.com/js/tryit.asp?filename=tryjs_array_sort_random I copied the myFunction () code form this example to randomize the character names

https://www.w3schools.com/js/tryit.asp?filename=tryjs_array_sort_alpha I copied the button code and p id="demo" code from this example to make a button that displays a character name onclick


https://fonts.google.com/specimen/Milonga?preview.text=Harry%20Potter&preview.text_type=custom&category=Display,Handwriting&query=milon#pairings I used Google Fonts to find Open Sans and Milonga for the text


